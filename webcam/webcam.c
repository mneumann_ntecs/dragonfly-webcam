#include <sys/param.h>  /* defines used in kernel.h */
#include <sys/types.h>
#include <sys/module.h>
#include <sys/kernel.h> /* types used in module initialization */
#include <sys/conf.h>   /* cdevsw struct */
#include <sys/uio.h>    /* uio struct */
#include <sys/malloc.h>
#include <sys/errno.h>
#include <sys/systm.h>
#include <sys/proc.h>
#include <vm/vm.h>
#include <vm/pmap.h>
#include "../test/videodev2.h"

static d_open_t      webcam_open;
static d_close_t     webcam_close;
static d_ioctl_t     webcam_ioctl;
static d_poll_t      webcam_poll;
static d_mmap_t      webcam_mmap;

static struct cdevsw webcam_cdevsw = {
	.d_version = D_VERSION,
	.d_open = webcam_open,
	.d_close = webcam_close,
  .d_ioctl = webcam_ioctl,
  .d_poll = webcam_poll,
  .d_mmap = webcam_mmap,
	.d_name = "webcam",
};

struct videobuf {
  uint8_t *data;
  int width;
  int height;
  size_t size;
};

typedef struct videobuf* videobuf_t;

static videobuf_t videobuf_alloc(int width, int height);
static void videobuf_fill(videobuf_t vbuf, uint8_t r, uint8_t g, uint8_t b);
static void videobuf_free(videobuf_t vbuf);
 
/*
const int PIXELFORMAT = V4L2_PIX_FMT_RGB24;
const char *PIXELFORMAT_NAME = "RGB 8:8:8";
const size_t BYTES_PER_PIXEL = 3;
*/

const int PIXELFORMAT = V4L2_PIX_FMT_YUYV;
const char *PIXELFORMAT_NAME = "16 YUV 4:2:2";
const size_t BYTES_PER_PIXEL = 2;


struct videobuf*
videobuf_alloc(int width, int height) {
  size_t size;
  struct videobuf* vbuf;
  uint8_t * data;

  size = (size_t)width * (size_t)height * BYTES_PER_PIXEL;
  vbuf = (struct videobuf*)malloc(sizeof(struct videobuf), M_DEVBUF, M_WAITOK);

  if (vbuf == NULL) {
    return NULL;
  }

  data = (uint8_t*)malloc(size, M_DEVBUF, M_WAITOK);

  if (data == NULL) {
    free(vbuf, M_DEVBUF);
    return NULL;
  }

  bzero(vbuf, sizeof(struct videobuf));

  vbuf->size = size;
  vbuf->width = width;
  vbuf->height = height;
  vbuf->data = data;

  return vbuf;
}


void
videobuf_free(struct videobuf* vbuf) {
  if (vbuf != NULL) {
    if (vbuf->data != NULL) {
      free(vbuf->data, M_DEVBUF);
    }
    bzero(vbuf, sizeof(struct videobuf));
    free(vbuf, M_DEVBUF);
  }
}
 
 
void
videobuf_fill(struct videobuf *vbuf, uint8_t r, uint8_t g, uint8_t b) {
  uint8_t *data = vbuf->data;

  if (PIXELFORMAT == V4L2_PIX_FMT_RGB24) {
    for (int y=0; y < vbuf->height; ++y) {
      for (int x=0; x < vbuf->width; ++x) {
        size_t off = ((y * vbuf->width) + x) * 3;
        data[off + 0] = r;
        data[off + 1] = g;
        data[off + 2] = b;
      }
    }
  }
  else if (PIXELFORMAT == V4L2_PIX_FMT_YUYV) {
    for (size_t i = 0; i < vbuf->size; ++i) {
      data[i] = b;
    }
  }
}

static struct cdev *webcam_dev;
static int ref = 0;

static int nbufs = 0;
static bool is_streaming = false;
static int frame_count = 0;

static videobuf_t bufs[2] = {NULL, NULL};

static int free_sz = 0;
static int full_sz = 0;
static int free_queue[2] = {-1, -1};
static int full_queue[2] = {-1, -1};

static int push_free(int idx) {
  if (free_sz < 2) {
    free_queue[free_sz++] = idx;
    return free_sz;
  }
  else {
    return -1;
  }
}

static int push_full(int idx) {
  if (full_sz < 2) {
    full_queue[full_sz++] = idx;
    return full_sz;
  }
  else {
    return -1;
  }
}

static int pop_front_free() {
  int front = -1;
  if (free_sz > 0) {
    front = free_queue[0];
    free_queue[0] = free_queue[1];
    free_queue[1] = -1;
    free_sz--;
  }
  return front;
}

static int pop_front_full() {
  int front = -1;
  if (full_sz > 0) {
    front = full_queue[0];
    full_queue[0] = full_queue[1];
    full_queue[1] = -1;
    free_sz--;
  }
  return front;
}

static int
webcam_loader(struct module *m, int what, void *arg)
{
  int error = 0;

  switch (what) {
  case MOD_LOAD:
    error = make_dev_p(MAKEDEV_CHECKNAME | MAKEDEV_WAITOK,
        &webcam_dev,
        &webcam_cdevsw,
        0,
        UID_ROOT,
        GID_VIDEO,
        0660,
        "video0");
    if (error != 0)
      break;

    printf("webcam KLD loaded.\n");
    break;
  case MOD_UNLOAD:
    destroy_dev(webcam_dev);
    printf("webcam KLD unloaded.\n");
    break;
  default:
    error = EOPNOTSUPP;
    break;
  }
  return(error);
}


static int
webcam_open(struct cdev *dev __unused, int oflags __unused, int devtype __unused,
    struct thread *td __unused)
{
	int error = 0;
  if (ref > 0) {
    return 1;
  }

  ref++;

	printf("%d: Opened device \"webcam\" successfully\n", ref);
	return (error);
}

static int
webcam_close(struct cdev *dev __unused, int fflag __unused, int devtype __unused,
    struct thread *td __unused)
{
	printf("%d: Closing device \"webcam\".\n", ref);

  nbufs = 0;

  --ref;

	return (0);
}

#define NOTSUP_IOC(ioc) \
  case ioc: \
    printf("%d: NOT SUPPORTED IOC: " # ioc "\n", ref); \
    error = ENOTSUP; \
    break;

static void inspect_pix_format(struct v4l2_pix_format *pix) {
  char *pixelformat = (char *)&pix->pixelformat;
  printf("width:\t%d\n", pix->width);
  printf("height:\t%d\n", pix->height);
  printf("pixelformat:\t%c%c%c%c\n", pixelformat[0], pixelformat[1],
         pixelformat[2], pixelformat[3]);
  printf("bytesperline:\t%d\n", pix->bytesperline);
  printf("sizeimage:\t%d\n", pix->sizeimage);
}


static int
webcam_ioctl(struct cdev *dev __unused, u_long cmd, caddr_t addr, int flag, struct thread *td)
{
  int error = 0;

  switch (cmd) {
    case VIDIOC_QUERYCAP:
      {
        struct v4l2_capability *cap;
        cap = (struct v4l2_capability*)addr;
        bzero(cap, sizeof(struct v4l2_capability));
        strncpy(cap->driver, "webcam", sizeof(((struct v4l2_capability*)0)->driver) - 1);
        strncpy(cap->card, "card", sizeof(((struct v4l2_capability*)0)->card) - 1);
        strncpy(cap->bus_info, "bus", sizeof(((struct v4l2_capability*)0)->bus_info) - 1);
        cap->version = 1;
        cap->capabilities = V4L2_CAP_VIDEO_CAPTURE |
          V4L2_CAP_STREAMING;
        cap->device_caps = V4L2_CAP_VIDEO_CAPTURE | V4L2_CAP_STREAMING;
      }
      printf("%d: VIDIOC_QUERYCAP = %d\n", ref, error);
      break;

    case VIDIOC_ENUMINPUT:
      {
        struct v4l2_input *input;
        input = (struct v4l2_input*)addr;
        if (input->index == 0) {
          strncpy(input->name, "Built in webcam", sizeof(((struct v4l2_input*)0)->name) - 1);
          input->type = V4L2_INPUT_TYPE_CAMERA; 
        } else {
          error = EINVAL;
        }
      }
      printf("%d: VIDIOC_ENUMINPUT = %d\n", ref, error);
      break;
    case VIDIOC_S_INPUT:
      {
        int *index;
        index = (int*)addr;
        if (*index == 0) {
          error = 0;
        } else {
          error = EINVAL;
        }
      }
      printf("%d: VIDIOC_S_INPUT = %d\n", ref, error);
      break;
    case VIDIOC_G_INPUT:
       {
        int *index;
        index = (int*)addr;
        *index = 0;
        error = 0;
      }
      printf("%d: VIDIOC_G_INPUT = %d\n", ref, error);
      break;

    case VIDIOC_ENUM_FMT:
      /** This MUST be implemented! */ 
      {
        struct v4l2_fmtdesc *fmtdesc;
        fmtdesc = (struct v4l2_fmtdesc*)addr;
        // type and index is initialized
        printf("%d: VIDIOC_ENUM_FMT type=%d, index=%d\n", ref, fmtdesc->type, fmtdesc->index);
        if (fmtdesc->type == V4L2_BUF_TYPE_VIDEO_CAPTURE
            && fmtdesc->index == 0) {
          fmtdesc->flags = 0;
          strncpy(fmtdesc->description, PIXELFORMAT_NAME,
              sizeof(((struct v4l2_fmtdesc*)0)->description) - 1);
          fmtdesc->pixelformat = PIXELFORMAT;
          error = 0;
        } else {
          printf("%d: type=%d, index=%d\n", ref, fmtdesc->type, fmtdesc->index);
          error = EINVAL;
        }
      }
      printf("%d: VIDIOC_ENUM_FMT = %d\n", ref, error);
      break;

    case VIDIOC_G_FMT:
      {
        struct v4l2_format *format;
        format = (struct v4l2_format*)addr;

        if (format->type == V4L2_BUF_TYPE_VIDEO_CAPTURE) {
          struct v4l2_pix_format *pix = &format->fmt.pix;
          pix->width = 640;
          pix->height = 480;
          pix->pixelformat = PIXELFORMAT;
          pix->field = V4L2_FIELD_NONE;
          pix->bytesperline = pix->width * BYTES_PER_PIXEL;
          pix->sizeimage = pix->bytesperline * pix->height;
          pix->colorspace = V4L2_COLORSPACE_SRGB;
          pix->priv = 0;
          pix->flags = 0;
          pix->ycbcr_enc = V4L2_YCBCR_ENC_DEFAULT;
          pix->hsv_enc = V4L2_HSV_ENC_256;  
          pix->quantization = V4L2_QUANTIZATION_DEFAULT;  
          pix->xfer_func = V4L2_XFER_FUNC_DEFAULT;  
          error = 0;
        }
        else {
          error = EINVAL;
        }
      }
      printf("%d: VIDIOC_G_FMT = %d\n", ref, error);
      break;

    case VIDIOC_TRY_FMT:
    case VIDIOC_S_FMT:
      {
        struct v4l2_format *format;
        format = (struct v4l2_format*)addr;

        inspect_pix_format(&format->fmt.pix);

        if (format->type == V4L2_BUF_TYPE_VIDEO_CAPTURE) {
          struct v4l2_pix_format *pix = &format->fmt.pix;
          pix->width = 640;
          pix->height = 480;
          pix->pixelformat = PIXELFORMAT;
          pix->field = V4L2_FIELD_NONE;
          pix->bytesperline = pix->width * BYTES_PER_PIXEL;
          pix->sizeimage = pix->bytesperline * pix->height;
          pix->colorspace = V4L2_COLORSPACE_SRGB;
          pix->priv = 0;
          pix->flags = 0;
          pix->ycbcr_enc = V4L2_YCBCR_ENC_DEFAULT;
          pix->hsv_enc = V4L2_HSV_ENC_256;  
          pix->quantization = V4L2_QUANTIZATION_DEFAULT;  
          pix->xfer_func = V4L2_XFER_FUNC_DEFAULT;  


          error = 0;
        }
        else {
          error = EINVAL;
        }
      }
      printf("%d: %s = %d\n", ref, 
          (cmd == VIDIOC_TRY_FMT) ? "VIDIOC_TRY_FMT" : "VIDIOC_S_FMT", error);
      break;

    case VIDIOC_S_PRIORITY:
      {
        error = 0;
      }
      printf("%d: VIDIOC_S_PRIORITY = %d\n", ref, error);
      break;

      /** These ioctl's shall return ENOTTY or EINVAL in case of webcams
       * where there is no meaning for standards.
       */
    case VIDIOC_G_STD:
    case VIDIOC_S_STD:
    case VIDIOC_QUERYSTD:
    case VIDIOC_ENUMSTD:
      {
        error = ENOTTY;
      }
      printf("%d: VIDIOC_*STD = %d\n", ref, error);
      break;

    case VIDIOC_QUERYCTRL:
      {
        error = EINVAL;
      }
      // printf("%d: VIDIOC_QUERYCTRL = %d\n", ref, error);
      break;


    case VIDIOC_CROPCAP:
      {
        struct v4l2_cropcap *cropcap;
        cropcap = (struct v4l2_cropcap*)addr;

        if (cropcap->type == V4L2_BUF_TYPE_VIDEO_CAPTURE) {
          cropcap->bounds.left = 0;
          cropcap->bounds.top = 0;
          cropcap->bounds.width = 640;
          cropcap->bounds.height = 480;

          cropcap->defrect.left = 0;
          cropcap->defrect.top = 0;
          cropcap->defrect.width = 640;
          cropcap->defrect.height = 480;

          cropcap->pixelaspect.numerator = 1;
          cropcap->pixelaspect.denominator = 1;

          error = 0;
        }
        else {
          error = EINVAL;
        }
      }
      printf("%d: VIDIOC_CROPCAP = %d\n", ref, error);
      break;

    case VIDIOC_G_PARM:
      {
        struct v4l2_streamparm *parm;
        parm = (struct v4l2_streamparm*)addr;
        if (parm->type == V4L2_BUF_TYPE_VIDEO_CAPTURE) {
          parm->parm.capture.capability = 0; // XXX
          parm->parm.capture.capturemode = 0; // XXX
          parm->parm.capture.extendedmode = 0;
          parm->parm.capture.readbuffers = 1;
          error = 0;
        }
        else {
          error = EINVAL;
        }
      }
      printf("%d: VIDIOC_G_PARM= %d\n", ref, error);
      break;

    case VIDIOC_ENUM_FRAMESIZES:
      {
        struct v4l2_frmsizeenum *frmsizeenum;
        frmsizeenum = (struct v4l2_frmsizeenum*)addr;
        if (frmsizeenum->index == 0 && frmsizeenum->pixel_format == PIXELFORMAT) {
          frmsizeenum->type = V4L2_FRMSIZE_TYPE_DISCRETE;
          frmsizeenum->discrete.width = 640;
          frmsizeenum->discrete.height = 480;
          error = 0;
        }
        else {
          error = EINVAL;
        }
      }
      printf("%d: VIDIOC_ENUM_FRAMESIZES = %d\n", ref, error);
      break;

    case VIDIOC_ENUM_FRAMEINTERVALS:
      {
        struct v4l2_frmivalenum *frmivalenum;
        frmivalenum = (struct v4l2_frmivalenum*)addr;
        if (frmivalenum->index == 0 && frmivalenum->pixel_format == PIXELFORMAT &&
            frmivalenum->width == 640 && frmivalenum->height == 480) {
          frmivalenum->type = V4L2_FRMSIZE_TYPE_DISCRETE;
          frmivalenum->discrete.numerator = 1;
          frmivalenum->discrete.denominator = 25;
          error = 0;
        }
        else {
          error = EINVAL;
        }
      }
      printf("%d: VIDIOC_ENUM_FRAMESIZES = %d\n", ref, error);
      break;

    case VIDIOC_REQBUFS:
      {
        struct v4l2_requestbuffers *reqbufs;
        reqbufs = (struct v4l2_requestbuffers*)addr;

        printf("[%d]: VIDIOC REQBUFS: ", ref);
        if (reqbufs->memory == V4L2_MEMORY_MMAP) {
          printf("MMAP\n");
        }
        else if (reqbufs->memory == V4L2_MEMORY_USERPTR) {
          printf("USERPTR\n");
        }
        else {
          printf("???\n");
        }

        if (reqbufs->type == V4L2_BUF_TYPE_VIDEO_CAPTURE &&
            reqbufs->memory == V4L2_MEMORY_MMAP) {
          /*
            for (int i = 0; i < nbufs; ++i) {
              videobuf_free(bufs[i]);
              bufs[i] = NULL;
            }
            */

          if (nbufs > 0) {
            printf("Already allocated\n");
            error = EINVAL; 
          } else {
            reqbufs->count = 2;
            nbufs = reqbufs->count;

            for (int i = 0; i < nbufs; ++i) {
              bufs[i] = videobuf_alloc(640, 480);
              free_queue[i] = -1;
              full_queue[i] = -1;
            }
            free_sz = 0;
            full_sz = 0;

            error = 0;
            reqbufs->capabilities = V4L2_BUF_CAP_SUPPORTS_MMAP;
          }
        }
        else {
          error = EINVAL;
        }
      }
      printf("[%d]: VIDIOC_REQBUFS = %d\n", ref, error);

      break;

   case VIDIOC_STREAMON:
      error = 0;
      is_streaming = true;
      break;

   case VIDIOC_STREAMOFF:
      error = 0;
      is_streaming = false;
      break;

    case VIDIOC_QBUF:
      {
        error = 0;
        struct v4l2_buffer *buffer;
        buffer = (struct v4l2_buffer*)addr;

        printf("VIDIOC QBUF: ");
        if (buffer->memory == V4L2_MEMORY_MMAP) {
          printf("MMAP\n");
        }
        else if (buffer->memory == V4L2_MEMORY_USERPTR) {
          printf("USERPTR\n");
        }
        else {
          printf("???\n");
        }



        if (buffer->type == V4L2_BUF_TYPE_VIDEO_CAPTURE &&
            buffer->memory == V4L2_MEMORY_MMAP && buffer->index < nbufs) {

          printf("queue buffer index=%d, length = %d\n", buffer->index, buffer->length);

          for (int i = 0; i < nbufs; ++i) {
            if (free_queue[i] == buffer->index) {
              printf("Buffer already enqueued\n");
              error = EINVAL;
            }
          }

          if (error == 0) {
            if (push_free(buffer->index) < 0) {
              error = EAGAIN;
            } else {
              buffer->flags &= ~V4L2_BUF_FLAG_DONE;
              buffer->flags |= V4L2_BUF_FLAG_MAPPED;
              buffer->flags |= V4L2_BUF_FLAG_QUEUED;
            }
          }

        } else {
          error = EINVAL; 
        }
      }
      printf("[%d]: VIDIOC_QBUF = %d\n", ref, error);
      break;

    case VIDIOC_DQBUF:
      {
        struct v4l2_buffer *buffer;
        buffer = (struct v4l2_buffer*)addr;

        printf("VIDIOC DQBUF: ");
        if (buffer->memory == V4L2_MEMORY_MMAP) {
          printf("MMAP\n");
        }
        else if (buffer->memory == V4L2_MEMORY_USERPTR) {
          printf("USERPTR\n");
        }
        else {
          printf("???\n");
        }

        if (buffer->type == V4L2_BUF_TYPE_VIDEO_CAPTURE &&
            buffer->memory == V4L2_MEMORY_MMAP) {

            printf("dequeuing buffer index=%d, length = %d\n", buffer->index, buffer->length);

            ++frame_count;

            int idx = pop_front_full();
            if (idx < 0) {
              idx = pop_front_free();
            }

            if (idx >= 0) {
              videobuf_fill(bufs[idx], 0, 0, (frame_count & 0xFF));
              buffer->index = idx;
              buffer->flags = 0;
              buffer->flags |= V4L2_BUF_FLAG_DONE;
              buffer->flags |= V4L2_BUF_FLAG_MAPPED;
              buffer->bytesused = bufs[idx]->size; 
              buffer->timestamp.tv_sec = frame_count / 50;
              buffer->timestamp.tv_usec = frame_count * 1000;
              buffer->timecode.type = V4L2_TC_TYPE_25FPS;
              buffer->timecode.flags = 0;
              buffer->timecode.frames = 25;
              buffer->timecode.seconds = frame_count / 50;
              buffer->timecode.minutes = 0;
              buffer->timecode.hours = 0;
              // buffer->timecode.userbits = 0;
              buffer->sequence =  frame_count;
              error = 0;
            } else {
              printf("AGAIN\n");
              error = EAGAIN;
            }
        } else {
          error = EINVAL;
        }
      }
      printf("%d: VIDIOC_DQBUF = %d\n", ref, error);
      break;

    case VIDIOC_QUERYBUF:
      {
        struct v4l2_buffer *buffer;
        buffer = (struct v4l2_buffer*)addr;

        printf("[%d] QUERYBUF (index=%d)\n", ref, buffer->index);

        if (buffer->type == V4L2_BUF_TYPE_VIDEO_CAPTURE &&
            buffer->index < nbufs) 
        {
            printf("index=%d, length = %d\n", buffer->index, buffer->length);

            buffer->memory = V4L2_MEMORY_MMAP;
            buffer->m.offset = buffer->index * bufs[buffer->index]->size;
            buffer->length = bufs[buffer->index]->size; 
            buffer->flags |= V4L2_BUF_FLAG_MAPPED;

            error = 0;
        } else {
          error = EINVAL;
        }
      }
      printf("%d: VIDIOC_QUERYBUF = %d\n", ref, error);

      break;

    NOTSUP_IOC(VIDIOC_G_FBUF)
    NOTSUP_IOC(VIDIOC_S_FBUF)
    NOTSUP_IOC(VIDIOC_OVERLAY)
    NOTSUP_IOC(VIDIOC_EXPBUF)
    NOTSUP_IOC(VIDIOC_S_PARM)
    NOTSUP_IOC(VIDIOC_G_CTRL)
    NOTSUP_IOC(VIDIOC_S_CTRL)
    NOTSUP_IOC(VIDIOC_QUERYMENU)
    NOTSUP_IOC(VIDIOC_G_CROP)
    NOTSUP_IOC(VIDIOC_S_CROP)
    NOTSUP_IOC(VIDIOC_G_JPEGCOMP)
    NOTSUP_IOC(VIDIOC_S_JPEGCOMP)
    NOTSUP_IOC(VIDIOC_G_PRIORITY)
    default:
      printf("%d: Unknown ioctl %ld (%d)\n", ref, cmd, (int)(cmd & 0xFF));
      error = ENOTSUP;
      break;
  }

  return error;
}


static int
webcam_poll(struct cdev *dev, int events, struct thread *td)
{
  printf("%d: Poll: events=%d\n", ref, events);
  printf("hz: %d\n", hz);
  pause("webcam_poll", 10);
  printf("Poll awake\n");
  return 1;

  /*
	int err;
	int revents;

	revents = 0;

	err = devfs_get_cdevpriv((void **)&priv);
	if (err != 0) {
		revents = POLLERR;
		return (revents);
	}

	if (SLIST_EMPTY(&priv->pins)) {
		revents = POLLHUP;
		return (revents);
	}

	if (events & (POLLIN | POLLRDNORM)) {
		if (priv->evidx_head != priv->evidx_tail)
			revents |= events & (POLLIN | POLLRDNORM);
		else
			selrecord(td, &priv->selinfo);
	}

	return (revents);
  */
}

static int
webcam_mmap(struct cdev *dev, vm_ooffset_t offset, vm_paddr_t *paddr,
    int nprot, vm_memattr_t *memattr)
{
  printf("[%d]: mmap offset=%ld\n", ref, offset);

  if (offset < bufs[0]->size) {
    *paddr = vtophys((char*)bufs[0]->data + offset);
  } else if (offset < 2 * bufs[0]->size) {
    *paddr = vtophys((char*)bufs[1]->data + offset - bufs[0]->size);
  } else {
    return EINVAL;
  }
  printf("*paddr=%ld\n", *paddr);
	return (0);
}

static moduledata_t webcam_mod = {
  "webcam",
  webcam_loader,
  NULL
};

DECLARE_MODULE(webcam, webcam_mod, SI_SUB_KLD, SI_ORDER_ANY);
