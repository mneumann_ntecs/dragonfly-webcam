#include "videodev2.h"
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/select.h>
#include <unistd.h>

void inspect_capability(struct v4l2_capability *cap) {
  printf("driver:\t%s\n", cap->driver);
  printf("card:\t%s\n", cap->card);
  printf("bus_info:\t%s\n", cap->bus_info);
  printf("version:\t%d\n", cap->version);
  printf("capabilities:\t");

  if (cap->capabilities & V4L2_CAP_VIDEO_CAPTURE)
    printf("CAPTURE ");
  if (cap->capabilities & V4L2_CAP_VIDEO_CAPTURE_MPLANE)
    printf("CAPTURE_MPLANE ");
  if (cap->capabilities & V4L2_CAP_READWRITE)
    printf("READWRITE ");
  if (cap->capabilities & V4L2_CAP_STREAMING)
    printf("STREAMING ");
  printf("\n");
}

void inspect_video_input(struct v4l2_input *input) {
  printf("index:\t%d\n", input->index);
  printf("name:\t%s\n", input->name);
  switch (input->type) {
  case V4L2_INPUT_TYPE_TUNER:
    printf("type:\tTUNER\n");
    break;
  case V4L2_INPUT_TYPE_CAMERA:
    printf("type:\tCAMERA\n");
    break;
  case V4L2_INPUT_TYPE_TOUCH:
    printf("type:\tTOUCH\n");
    break;
  default:
    printf("type:\t<unknown>\n");
  }
}

void inspect_pix_format(struct v4l2_pix_format *pix) {
  char *pixelformat = (char *)&pix->pixelformat;
  printf("width:\t%d\n", pix->width);
  printf("height:\t%d\n", pix->height);
  printf("pixelformat:\t%c%c%c%c\n", pixelformat[0], pixelformat[1],
         pixelformat[2], pixelformat[3]);
  printf("bytesperline:\t%d\n", pix->bytesperline);
  printf("sizeimage:\t%d\n", pix->sizeimage);
}

int main(int argc, char **argv) {
  int i;
  int fd;
  int error;
  struct v4l2_input input;
  struct v4l2_capability cap;
  struct v4l2_format format;
  void *buffer;

  fd = open("/dev/video0", O_RDWR | O_NONBLOCK, 0);
  if (fd < 0) {
    printf("Failed to open /dev/video0\n");
    return 1;
  }

  /** Query device capabilities
   */
  bzero(&cap, sizeof(struct v4l2_capability));
  error = ioctl(fd, VIDIOC_QUERYCAP, &cap);
  if (error == 0) {
    inspect_capability(&cap);
  } else {
    return 1;
  }

  printf("----------------------------------\n");

  /** Query video inputs
   */
  for (i = 0; i < 10; ++i) {
    bzero(&input, sizeof(struct v4l2_input));
    input.index = i;

    error = ioctl(fd, VIDIOC_ENUMINPUT, &input);
    if (error == 0) {
      inspect_video_input(&input);
    } else {
      break;
    }
  }

  printf("----------------------------------\n");

  /** Set video input
   */
  i = 0;
  error = ioctl(fd, VIDIOC_S_INPUT, &i);
  if (error == 0) {
    printf("Input %d selected\n", i);
  }

  printf("----------------------------------\n");

  /** Get data format
   */
  bzero(&format, sizeof(struct v4l2_format));
  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  error = ioctl(fd, VIDIOC_G_FMT, &format);
  if (error == 0) {
    inspect_pix_format(&format.fmt.pix);
  } else {
    return 1;
  }

  /** Force data format
   */
  /*
  bzero(&format, sizeof(struct v4l2_format));
  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  format.fmt.pix.width = 640;
  format.fmt.pix.height = 480;
  format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
  format.fmt.pix.field       = V4L2_FIELD_INTERLACED;
  error = ioctl(fd, VIDIOC_S_FMT, &format);
  if (error == 0) {
    int min = format.fmt.pix.width * 2;
    if (format.fmt.pix.bytesperline < min)
      format.fmt.pix.bytesperline = min;
    min = format.fmt.pix.bytesperline * format.fmt.pix.height;
    if (format.fmt.pix.sizeimage < min)
      format.fmt.pix.sizeimage = min;

    inspect_pix_format(&format.fmt.pix);
  } else {
    return 1;
  }
  */

  /**
   * Set device priority
   */
  enum v4l2_priority prio = V4L2_PRIORITY_RECORD;
  error = ioctl(fd, VIDIOC_S_PRIORITY, &prio);
  if (error == 0) {
    printf("Set device priority to RECORD\n");
  } else {
    printf("Failed to set device priority to RECORD\n");
    return 1;
  }

  size_t bufsiz = format.fmt.pix.sizeimage;
  buffer = malloc(bufsiz);
  if (buffer == NULL) {
    printf("Failed to allocate buffer\n");
    return 1;
  }
  printf("buffer: %p\n", buffer);
  printf("buffersiz: %zu\n", bufsiz);

  for (;;) {
    fd_set fds;
    struct timeval tv = {.tv_sec = 2, .tv_usec = 0};
    int r;

    FD_ZERO(&fds);
    FD_SET(fd, &fds);

    r = select(fd + 1, &fds, NULL, NULL, &tv);
    if (r == 0) {
      printf("Timeout\n");
      break;
    }
    if (r == -1) {
      if (errno == EINTR) {
        continue;
      }
      perror("select");
      return 1;
    }

    error = read(fd, buffer, bufsiz);
    if (error == 0) {
      printf("EOF\n");
      break;
    } else if (error > 0) {
      printf("GOT %d\n", error);
    } else if (error == -1) {
      switch (errno) {
      case EAGAIN:
        printf("EAGAIN\n");
        break;
      case EIO:
        printf("EIO\n");
        // Ignore
        break;
      default:
        perror("read");
        return 1;
      }
    }
  }

  return 0;
}
