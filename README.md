# Webcam support for DragonflyBSD

This project aims at adding rudimentary support for USB-based `uvc`-class
webcams to the DragonflyBSD kernel for the purpose of video-conferencing.

The device interface will be compatible with (a subset) of [Video for Linux 2
(V4L2)][v4l2].

## Roadmap

* Milestone 1: The `/dev/video0` shows a test screen. That's useful on it's
  own, because [Jitsi Meet][jitsi] doesn't work in Firefox without a camera
  attached (seems to work on chrome), so you cannot even attent a video
  conference with Firefox.

* Milestone 2: Driver works with my Chicony USB2.0 Camera, either by consuming
  the USB video device in-kernel, or by allowing a userlevel application to
  inject video data. There is `libuvc` that makes communication with
  uvc-based webcams easy on the userlevel side. Note: The outcome of Milestone
  2 is "Just works for me".

[v4l2]: https://www.kernel.org/doc/html/v4.10/media/uapi/v4l/v4l2.html
[jitsi]: https://meet.jit.si
